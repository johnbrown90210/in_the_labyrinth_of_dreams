CODE ..\bin\labyri~1.ulx

PICTURE COVER ..\media\cover.png

AUTHOR Alexey Galkin

COPYRIGHT Alexey Galkin <johnbrown>, 2019

RELEASE 1

NOTE See more details at https://gitlab.com/johnbrown90210/in_the_labyrinth_of_dreams
